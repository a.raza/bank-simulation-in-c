/***********************************************************************
implementation file	: Cashier.c
Author				: <Costas Chatzopoulos -1115201300202- 21/03/2015>
Purpose				: Ylopoiisi tamia
Revision			: <Costas Chatzopoulos -1115201300202- 22/03/2015>
***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "Cashier.h"

int cashierAvailable(Cashier* target)
{
//    printf("\nisAvailable: %i\n", target->isAvailable);
    return target->isAvailable;
}

void addOneOccupancy(Cashier* target)
{
//    printf("\n occupied: %i\n", target->occupied);
    target->occupied++;
}

void newCashier(Cashier* nCashier)
{
    nCashier = malloc(sizeof(Cashier));

    nCashier->isAvailable = 1;
    nCashier->occupied = 0;
    nCashier->available = 0;
    nCashier->customersServed = 0;
    nCashier->withCustomer = 0;
    Customer* customer = malloc(sizeof(Customer));
    nCashier->c = customer;
}

void remCashier(Cashier* target)
{
    target->available = 0;
    remCustomer(target->c);
    target->customersServed = 0;
    target->isAvailable = 1;
    target->occupied = 0;
    target->withCustomer = 0;
}

void addCurrentCustomer(Cashier* target, Customer* current)
{
    target->c = current;
}

Customer* getCurrentCustomer(Cashier* target)
{
    return target->c;
}

void addOneAvailability(Cashier* target)
{
    target->available++;
}

void addOneCustomerServed(Cashier* target)
{
    target->customersServed++;
}

void assignWithCustomerValue(Cashier* target, int value)
{
    target->withCustomer = value;
}

void subOneWithCustomer(Cashier* target)
{
    target->withCustomer--;
}

int getWithCustomerValue(Cashier* target)
{
    return target->withCustomer;
}

void triggerAvailability(Cashier* target)
{
    if(target->isAvailable == 1)
    {
        target->isAvailable = 0;
    }
    else
    {
        target->isAvailable = 1;
    }
}
