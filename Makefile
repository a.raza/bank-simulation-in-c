All:bank
bank:main.o Cashier.o Customer.o
	gcc main.o Cashier.o Customer.o -o bank
main.o:
	gcc -c main.c
Cashier.o:
	gcc -c Cashier.c
Customer.o:
	gcc -c Customer.c
clean:
	rm -rf *o bank
#DEPENDENCIES: