#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include "Cashier.h"
#include "Customer.h"

#define MAXCHAR 32

enum ValueState
{
    INT,
    DOUBLE
};

double poissonRandomValue(double mean);

double poissonRandomValue(double mean)
{
    int n = 0;
    double limit;
    
    //    random number
    double x;
    
    limit = exp(-mean);
    x = rand() % ((int)(mean) + 1 - 0) + 0;
    while (x > limit)
    {
        n++;
        x *= rand() % ((int)(mean) + 1 - 0) + 0;
    }
    return n;
}

float avgWaitTime(int sum, int customerLen);
float avgWaitTime(int sum, int customerLen)
{
    float avg = 0.0;
    
    avg = sum/customerLen;
    
    return avg;
}

double getValidValue(char *st, enum ValueState state);

double getValidValue(char *st, enum ValueState state)
{
    char str[MAXCHAR];
    int index = 0;
    char trigger = 'f';
    
    int decimalPoint = 0;
    
    while (trigger == 'f')
    {
        printf("%s", st);
        scanf("%s", str);
        
        for(index = 0; index < strlen(str); index++)
        {
            if(isdigit(str[index]) == 0)
            {
                if(decimalPoint != 0 && str[index] != '.')
                {
                    printf("\n Invalid Input \n");
                    break;
                }
                else
                {
                    decimalPoint = 1;
                }
            }
            else
            {
                if(index == strlen(str)-1)
                {
                    trigger = 't';
                }
            }
        }
    }
    
    if(trigger == 't')
    {
        if(state == DOUBLE)
            return atoi(str);
        else
            return (int)atoi(str);
    }
    else
        return -1;
}


int bankProcessing(int cashierLen, Cashier *cashier[], int customerLen, Customer *customers[], int simTime, int avgServise);

int bankProcessing(int cashierLen, Cashier *cashier[], int customerLen, Customer *customers[], int simTime, int avgServise)
{
    
    int i, j, totalServed = 0;
    
    for (j = 0; j < cashierLen; j++)
    {
        if(customerLen == 0)
        {
            break;
        }
        
        if(cashier[j]->isAvailable == 1)
        {
            for(i = 0; i < customerLen; i++)
            {
                if((simTime == 0 && customers[i]->isBeingServed == 0)
                   ||
                   (customers[i]->isBeingServed == 0 && customers[i]->entred > 0))
                {
                    
                    int moment =  poissonRandomValue(avgServise);
                    cashier[j]->customersServed++;
                    cashier[j]->withCustomer = moment;
                    customers[i]->served = moment;
                    cashier[j]->c = customers[i];
                    cashier[j]->c->served = moment;
                    cashier[j]->c->entred = customers[i]->entred;
                    cashier[j]->c->WaitTimeInQ = customers[i]->WaitTimeInQ;
                    customers[i]->isBeingServed = 1;
                    cashier[j]->isAvailable = 0;
                    break;
                }
            }
            
        }
    }
    
    for (j = 0; j < customerLen; j++)
    {
        if(customers[j]->isBeingServed == 0)
        {
            addOneOnWaiting(customers[j]);
        }
    }
    
    for (j = 0 ; j < cashierLen; j++)
    {
        
        if(cashierAvailable(cashier[j]) == 1)
        {
            cashier[j]->available++;
        }
        else
        {
            cashier[j]->occupied++;
            cashier[j]->withCustomer--;
            if(cashier[j]->withCustomer == 0)
            {
                printf("\n --------------- ");
                printf("\n Entered at               : %i", cashier[j]->c->entred);
                printf("\n Waited                   : %i", cashier[j]->c->WaitTimeInQ);
                printf("\n Served in                : %i", cashier[j]->c->served);
                printf("\n Total Time spent in Bank :  %i", (cashier[j]->c->WaitTimeInQ+cashier[j]->c->served));
                printf("\n Served By Cashier Number : %i", j);
                printf("\n --------------- ");
                cashier[j]->isAvailable = 1;
                totalServed++;
                remCustomer(cashier[j]->c);
            }
        }
    }
    
    return totalServed;
}

int main(void)
{
    int cashierLen = 0;
    int customerLen = 0;
    Customer *customers[customerLen];
    int totalServed = 0;
    int avgServise = 0;
    float pbArrival = 0, percentage = 0;
    int simLimit = 0;
    int extratime = 0;
    int simTime = 0;
    int arrival = 0;
    int i = 0;
    int sum = 0;
    
    while (simLimit < 30 || simLimit >= 100)
    {
        simLimit = getValidValue("\nPlease provide time of the simulateion\n(Should be above 30 and less than 100): ", INT);
        
        if(simLimit < 30 || simLimit >= 100)
            printf("\nPlease, the input should be above 30 and less than 100.");
    }
    
    while (cashierLen < 2 || cashierLen >= 20)
    {
        cashierLen = (int)getValidValue("\nPlease provide number of cashiers: \n (Should be atleast 2 and less than 20):", INT);
    
        if (cashierLen < 2 || cashierLen >= 20)
            printf("\nPlease, the input should be atleast 2 and less than 20.");

    }
    
    //Create Cashiers
    Cashier* cashier[cashierLen];
    
    for(i = 0; i < cashierLen; i++)
    {
        cashier[i] = (Cashier*)malloc(sizeof(Cashier));
        cashier[i]->c = (Customer*)malloc(sizeof(Customer));
        cashier[i]->isAvailable = 1;
        cashier[i]->occupied = 0;
        cashier[i]->withCustomer = 0;
        cashier[i]->available = 0;
        cashier[i]->customersServed = 0;
    }
    
    do
    {
        avgServise = getValidValue("\nPlease provide average service time for each cashier:", INT);
        
    }while ( !(avgServise > 0) );
    
    do
    {
        pbArrival = getValidValue("\nPlease provide average duration between two arrivals\n(Between 2 to 100): ", DOUBLE);
        
    }while (!(pbArrival >= 2 && pbArrival <= 100));
    
    
    printf("\n-----\nThe simulation time is %u minutes.", simLimit);
    printf("\nAverage duration between two arrivals is %f", pbArrival);
    printf("\nThe maximum minutes of service time in cashier for a customer is %d minutes.", avgServise);
    printf("\nThe number of cashiers of bank: %d\n-----\n", cashierLen);
    
    
    arrival = poissonRandomValue(pbArrival);
    
    //Create Customers
    for (simTime = 0; simTime < simLimit; simTime++)
    {
        if(arrival == simTime)
        {
            arrival += poissonRandomValue(pbArrival);
            if(arrival > simLimit)
                break;
            customers[customerLen] = (Customer*)malloc(sizeof(Customer));
            customers[customerLen]->entred = simTime;
            customers[customerLen]->customerNumber = customerLen;
            customers[customerLen]->isBeingServed = 0;
            customers[customerLen]->served = 0;
            customers[customerLen]->WaitTimeInQ = 0;
            
            customerLen++;
        }
        
        totalServed += bankProcessing( cashierLen, cashier,  customerLen,  customers,  simTime,  avgServise);
        
    }

    while (totalServed != customerLen)
    {
        totalServed += bankProcessing( cashierLen, cashier,  customerLen,  customers,  simTime,  avgServise);
        
        extratime++;
    }

    printf("\n +++ Total Served: %i\n",totalServed);
    printf("\n +++ Customer Len: %i\n",customerLen);
    
    for (i = 0; i < cashierLen; i++)
    {
        printf("\n ----- \n");
        printf("\n Cashier: %i\n", i);
        printf("\n Cashier Busy Time: %i\n", cashier[i]->occupied);
        printf("\n Cashier Free Time: %i\n", cashier[i]->available);
        printf("\n Cashier Number Of Customers Served: %i\n", cashier[i]->customersServed);
        float totalOcRate =cashier[i]->occupied;
        percentage = totalOcRate/(simLimit+extratime)*100;
        printf("\n Cashier Busy Time in Percentage: %f\n", percentage);
        printf("\n ----- \n");
    }
    
    printf("\n ----- \n");
    printf("\n Total Customer Served: %i \n", totalServed);
    printf("\n Actual duration of the simulation: %i \n", (simLimit+extratime));
    
    for (i = 0; i < customerLen; i++)
    {
        printf("\n Customer %i :%i \n", i, ((Customer*)customers)[i].WaitTimeInQ);
        sum += ((Customer*)customers)[i].WaitTimeInQ;
    }
    
    printf("\n Average waiting time for customers: %f \n", avgWaitTime(sum, customerLen));
    printf("\n ----- \n");
   
    return 0;
}
