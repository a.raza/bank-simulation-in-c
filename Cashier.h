
#ifndef __cashier__ 
#define __cashier__

#include <stdio.h>
#include "Customer.h"

typedef struct
{
    int isAvailable; // to check if Cashier is available or not
    int occupied; // time when the cashier was working to serve customer
    int available; // time when cashier was free
    int customersServed; // number of customer served
    int withCustomer; // time which should take to serve current customer
    Customer* c; // will be the customer that is being served

} Cashier;

void newCashier(Cashier* nCashier);
void remCashier(Cashier* target);
void addCurrentCustomer(Cashier* target, Customer* current);
Customer* getCurrentCustomer(Cashier* target);
void addOneAvailability(Cashier* target);
int cashierAvailable(Cashier* target);
void addOneOccupancy(Cashier* target);
void addOneCustomerServed(Cashier* target);
void assignWithCustomerValue(Cashier* target, int value);
void subOneWithCustomer(Cashier* target);
int getWithCustomerValue(Cashier* target);
void triggerAvailability(Cashier* target);
#endif
