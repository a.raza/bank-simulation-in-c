
#ifndef __Customer__ 
#define __Customer__

#include <stdio.h>

typedef struct Customer
{
    int customerNumber;
    int entred;
	int served;     
    int WaitTimeInQ;
    int isBeingServed;
    
} Customer;

void createCustomer(Customer* newCustomer);
void remCustomer(Customer* target);
void addOneOnWaiting(Customer* target);
void assignServedTime(Customer* target, int value);
void triggerBeingServed(Customer* target);
int beingServed(Customer* target);
#endif
