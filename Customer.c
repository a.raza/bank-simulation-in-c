#include <stdio.h>
#include <stdlib.h>
#include "Customer.h"

void createCustomer(Customer* newCustomer)
{
    newCustomer = (Customer*)malloc(sizeof(Customer));

    newCustomer->customerNumber = 0;
    newCustomer->entred = 0;
    newCustomer->isBeingServed = 0;
    newCustomer->served = 0;
    newCustomer->WaitTimeInQ = 0;
}

void remCustomer(Customer* target)
{
    target->customerNumber = 0;
    target->entred = 0;
    target->served = 0;
    target->WaitTimeInQ = 0;
    target->isBeingServed = 0;
}

void addOneOnWaiting(Customer* target)
{
    target->WaitTimeInQ++;
}

void assignServedTime(Customer* target, int value)
{
    target->served = value;
}

void triggerBeingServed(Customer* target)
{
    if(target->isBeingServed == 1)
    {
        target->isBeingServed = 0;
    }
    else
    {
        target->isBeingServed = 1;
    }
}

int beingServed(Customer* target)
{
    return target->isBeingServed;
}
